//
//  FilterVC.m
//  avispl
//
//  Created by Faizan on 07/09/16.
//  Copyright © 2016 iShadowX. All rights reserved.
//

#import "FilterVC.h"
#import "cellOneVC.h"
#import "WebserviceCall.h"
#import "MBProgressHUD.h"
#import "avispl-Swift.h"

@interface FilterVC () <UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *table1;
@property (weak, nonatomic) IBOutlet UITableView *table2;
@property (weak, nonatomic) IBOutlet UITableView *table3;
@property (weak, nonatomic) IBOutlet UITableView *table4;
@property (nonatomic,strong) NSMutableArray *arrTableOne;
@property (nonatomic,strong) NSMutableArray *arrTableTwo;
@property (nonatomic,strong) NSMutableArray *arrTableThree;
@property (nonatomic,strong) NSMutableArray *arrTableFour;
@property (nonatomic,strong) NSMutableArray *arrDelegate;
@property (strong, nonatomic) NSMutableArray *selectedApps;
@property (strong, nonatomic) NSMutableArray *selectedMarkets;
@property (strong, nonatomic) NSMutableArray *selectedOfices;
@property (strong, nonatomic) NSMutableArray *selectedClients;
@property (strong, nonatomic) NSMutableDictionary *selectedData;
@property (strong, nonatomic) NSMutableArray *arrMultiSelectTable1;
@property (strong, nonatomic) NSMutableArray *arrMultiSelectTable2;
@property (strong, nonatomic) NSMutableArray *arrMultiSelectTable3;
@property (strong, nonatomic) NSMutableArray *arrMultiSelectTable4;

@end

@implementation FilterVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.selectedApps = [[NSMutableArray alloc]init ];
    self.selectedMarkets = [[NSMutableArray alloc]init ];
    self.selectedOfices = [[NSMutableArray alloc]init ];
    self.selectedClients = [[NSMutableArray alloc]init ];
    self.selectedData = [[NSMutableDictionary alloc]init ];
    self.arrMultiSelectTable1 = [[NSMutableArray alloc]init ];
    self.arrMultiSelectTable2 = [[NSMutableArray alloc]init ];
    self.arrMultiSelectTable3 = [[NSMutableArray alloc]init ];
    self.arrMultiSelectTable4 = [[NSMutableArray alloc]init ];

    
    [self.selectedData setValue:@"" forKey:@"AppId"];
    [self.selectedData setValue:@"" forKey:@"AppName"];
    [self.selectedData setValue:@"" forKey:@"MarketId"];
    [self.selectedData setValue:@"" forKey:@"MarketName"];
    [self.selectedData setValue:@"" forKey:@"OfficeId"];
    [self.selectedData setValue:@"" forKey:@"OfficeName"];
    [self.selectedData setValue:@"" forKey:@"ClientId"];
    [self.selectedData setValue:@"" forKey:@"ClientName"];
    
    
    [WebserviceCall fetchServerResponse:nil callBackSelector:@selector(responseApps:) andDelegate:self andwebMethod:@"GetApplications" andWebServiceMaster:@"GalleryService.asmx"];
    
    [WebserviceCall fetchServerResponse:nil callBackSelector:@selector(responseMarkets:) andDelegate:self andwebMethod:@"GetMarkets" andWebServiceMaster:@"GalleryService.asmx"];
    
    [WebserviceCall fetchServerResponse:nil callBackSelector:@selector(responseOffice:) andDelegate:self andwebMethod:@"GetOffices" andWebServiceMaster:@"GalleryService.asmx"];
    
    [WebserviceCall fetchServerResponse:nil callBackSelector:@selector(responseClient:) andDelegate:self andwebMethod:@"GetClients" andWebServiceMaster:@"GalleryService.asmx"];
    

    
    [MBProgressHUD showHUDAddedTo:self.view animated:true];
    self.preferredContentSize = CGSizeMake(900, 600);
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



-(void)responseApps:(id)response{
    
    self.arrTableOne = [[NSMutableArray alloc]initWithArray:[response valueForKey:@"ApplicationModel"]];
    [self.table1 reloadData];
 
    
//    [WebserviceCall fetchServerResponse:nil callBackSelector:@selector(responseMarkets:) andDelegate:self andwebMethod:@"GetMarkets" andWebServiceMaster:@"GalleryService.asmx"];
    
}

-(void)responseMarkets:(id)response{
    
    self.arrTableTwo = [[NSMutableArray alloc]initWithArray:[response valueForKey:@"MarketModel"]];
    [self.table2 reloadData];
    
//    [WebserviceCall fetchServerResponse:nil callBackSelector:@selector(responseOffice:) andDelegate:self andwebMethod:@"GetOffices" andWebServiceMaster:@"GalleryService.asmx"];
    
    
}

-(void)responseOffice:(id)response{
    
    self.arrTableThree = [[NSMutableArray alloc]initWithArray:[response valueForKey:@"OfficeModel"]];
    [self.table3 reloadData];
    
//    [WebserviceCall fetchServerResponse:nil callBackSelector:@selector(responseClient:) andDelegate:self andwebMethod:@"GetClients" andWebServiceMaster:@"GalleryService.asmx"];
//    
    
    
    
}

-(void)responseClient:(id)response{
    
    [MBProgressHUD hideHUDForView:self.view animated:true];
    self.arrTableFour = [[NSMutableArray alloc]initWithArray:[response valueForKey:@"ClientModel"]];
    [self.table4 reloadData];
    
    
    
    
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSInteger count = 1;
    
    if (tableView == _table1) {
        count = self.arrTableOne.count;
    }
    if (tableView == _table2) {
        count = self.arrTableTwo.count;
    }
    if (tableView == _table3) {
        count = self.arrTableThree.count;
    }
    if (tableView == _table4) {
        count = self.arrTableFour.count;
    }
    
    return count;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    cellOneVC *cell = [tableView dequeueReusableCellWithIdentifier:@""];
    
    if (tableView == _table1) {
        
        NSDictionary *dic = [self.arrTableOne objectAtIndex:indexPath.row];
        
        static NSString *CellIdentifier = @"tableOneCell";
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        cell.lblCellOne.text = [dic valueForKeyPath:@"Name.text"];
//        
//        if([[self.selectedData valueForKey:@"AppId"] isEqualToString:[dic valueForKeyPath:@"ApplicationId.text"]])
//        {
//            cell.accessoryType = UITableViewCellAccessoryCheckmark;
//        }else{
//            cell.accessoryType = UITableViewCellAccessoryNone;
//        }
        cell.accessoryType = UITableViewCellAccessoryNone;

        for (int x = 0 ; self.arrMultiSelectTable1.count>x; x++) {
            NSDictionary *dico = [self.arrMultiSelectTable1 objectAtIndex:x];
            

            
            if ([[dico valueForKeyPath:@"ApplicationId.text"]  isEqualToString:[dic valueForKeyPath:@"ApplicationId.text"] ]) {
                
                cell.accessoryType = UITableViewCellAccessoryCheckmark;
                break;
            }
        }
    }else if (tableView == _table2) {
        
        NSDictionary *dic = [self.arrTableTwo objectAtIndex:indexPath.row];
        
        static NSString *CellIdentifier = @"tableTwoCell";
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        cell.lblCellTwo.text = [dic valueForKeyPath:@"Name.text"];
        
//        if([[self.selectedData valueForKey:@"MarketId"]isEqualToString:[dic valueForKeyPath:@"MarketId.text"]])
//        {
//            cell.accessoryType = UITableViewCellAccessoryCheckmark;
//        }else{
//            cell.accessoryType = UITableViewCellAccessoryNone;
//        }

        cell.accessoryType = UITableViewCellAccessoryNone;
        
        for (int y = 0 ; self.arrMultiSelectTable2.count>y; y++) {
            NSDictionary *dico = [self.arrMultiSelectTable2 objectAtIndex:y];
            
            
            if ([[dico valueForKeyPath:@"MarketId.text"]  isEqualToString:[dic valueForKeyPath:@"MarketId.text"] ]) {
                
                cell.accessoryType = UITableViewCellAccessoryCheckmark;
                break;
            }

        
        
        }
        
    }else if (tableView == _table3) {
        
        NSDictionary *dic = [self.arrTableThree objectAtIndex:indexPath.row];
        static NSString *CellIdentifier = @"tableThreeCell";
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        cell.lblCellThree.text = [NSString stringWithFormat:@"%@, %@, %@",[self cellvalues:[dic valueForKeyPath:@"City.text"]],[self cellvalues:[dic valueForKeyPath:@"StateProvince.text"]],[dic valueForKeyPath:@"Country.text"] ];
        
//        if([[self.selectedData valueForKey:@"OfficeId"] isEqualToString:[dic valueForKeyPath:@"OfficeId.text"]])
//        {
//            cell.accessoryType = UITableViewCellAccessoryCheckmark;
//        }else{
//            cell.accessoryType = UITableViewCellAccessoryNone;
//        }

        cell.accessoryType = UITableViewCellAccessoryNone;
        
        for (int x = 0 ; self.arrMultiSelectTable3.count>x; x++) {
            NSDictionary *dico = [self.arrMultiSelectTable3 objectAtIndex:x];
            

            
            if ([[dico valueForKeyPath:@"OfficeId.text"]  isEqualToString:[dic valueForKeyPath:@"OfficeId.text"] ]) {
                
                cell.accessoryType = UITableViewCellAccessoryCheckmark;
                break;
            }

    
    
        }
    
    
    }else if (tableView == _table4) {
        
        NSDictionary *dic = [self.arrTableFour objectAtIndex:indexPath.row];
        static NSString *CellIdentifier = @"tableFourCell";
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        cell.lblCellTwo.text = [NSString stringWithFormat:@"%@, %@",[dic valueForKeyPath:@"Name.text"],[dic valueForKeyPath:@"City.text"] ];
        
//        if([[self.selectedData valueForKey:@"ClientId"] isEqualToString:[dic valueForKeyPath:@"ClientId.text"]])
//        {
//            cell.accessoryType = UITableViewCellAccessoryCheckmark;
//        }else{
//            cell.accessoryType = UITableViewCellAccessoryNone;
//        }
        cell.accessoryType = UITableViewCellAccessoryNone;
        
        for (int x = 0 ; self.arrMultiSelectTable4.count>x; x++) {
            NSDictionary *dico = [self.arrMultiSelectTable4 objectAtIndex:x];
            
            
            if ([[dico valueForKeyPath:@"ClientId.text"]  isEqualToString:[dic valueForKeyPath:@"ClientId.text"] ]) {
                
                cell.accessoryType = UITableViewCellAccessoryCheckmark;
                break;
            }
        }
    
    
    
    }
    
    return cell;
    
}

-(NSString*)cellvalues: (NSString*)passvalue{

    if (passvalue == nil) {
        
        return @"";
    }else{
    
        return passvalue;
    }


}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (tableView == _table1) {
        
        NSDictionary *dic = [self.arrTableOne objectAtIndex:indexPath.row];
        
        
        
        NSString *selectedAppsId = [NSString stringWithFormat:@"%@", [dic valueForKeyPath:@"ApplicationId.text"]];
        NSString *selectedAppsName = [NSString stringWithFormat:@"%@", [dic valueForKeyPath:@"Name.text"]];
                
//        if ([[self.selectedData valueForKey:@"AppId"] isEqualToString:@""] && [[self.selectedData valueForKey:@"AppName"] isEqualToString:@""]) {
        
            [self.selectedData setValue:selectedAppsId forKey:@"AppId"];
            [self.selectedData setValue:selectedAppsName forKey:@"AppName"];
        
        BOOL isAllReadyExist = NO;
        
        for (int x = 0 ; self.arrMultiSelectTable1.count>x; x++) {
            NSDictionary *dico = [self.arrMultiSelectTable1 objectAtIndex:x];
            
                     if ([[dico valueForKeyPath:@"ApplicationId.text"]  isEqualToString:[dic valueForKeyPath:@"ApplicationId.text"] ]) {
                
                isAllReadyExist = YES;
                
                [self.arrMultiSelectTable1 removeObjectAtIndex:x];
                break;
            }
        }
        
        if (!isAllReadyExist) {
            [self.arrMultiSelectTable1 addObject:dic];
            

        }

        
        


        
//        }else{
//            
//            [self.selectedData setValue:@"" forKey:@"AppId"];
//            [self.selectedData setValue:@"" forKey:@"AppName"];
//            
//        }
//        
        
        [self.table1 reloadData];
        
    }else if (tableView == _table2) {
        
        NSDictionary *dic = [self.arrTableTwo objectAtIndex:indexPath.row];
        NSString *selectedMarketId = [NSString stringWithFormat:@"%@", [dic valueForKeyPath:@"MarketId.text"]];
        NSString *selectedMarketName = [NSString stringWithFormat:@"%@", [dic valueForKeyPath:@"Name.text"]];
        
//        if ([[self.selectedData valueForKey:@"MarketId"] isEqualToString:@""] && [[self.selectedData valueForKey:@"MarketName"] isEqualToString:@""]) {
        
            [self.selectedData setValue:selectedMarketId forKey:@"MarketId"];
            [self.selectedData setValue:selectedMarketName forKey:@"MarketName"];
        BOOL isAllReadyExist = NO;
        
        for (int x = 0 ; self.arrMultiSelectTable2.count>x; x++) {
            NSDictionary *dico = [self.arrMultiSelectTable2 objectAtIndex:x];
            
            
            if ([[dico valueForKeyPath:@"MarketId.text"]  isEqualToString:[dic valueForKeyPath:@"MarketId.text"] ]) {
                
                isAllReadyExist = YES;
                
                [self.arrMultiSelectTable2 removeObjectAtIndex:x];
                break;
            }
        }
        
        if (!isAllReadyExist) {
            [self.arrMultiSelectTable2 addObject:dic];
            
        }

//        }else{
//            
//            [self.selectedData setValue:@"" forKey:@"MarketId"];
//            [self.selectedData setValue:@"" forKey:@"MarketName"];
//            
//        }
       
        [self.table2 reloadData];
        
    } else if (tableView == _table3) {
        
        NSDictionary *dic = [self.arrTableThree objectAtIndex:indexPath.row];
        NSString *selectedOfficeId = [NSString stringWithFormat:@"%@", [dic valueForKeyPath:@"OfficeId.text"]];
        NSString *selectedOfficeName = [NSString stringWithFormat:@"%@", [dic valueForKeyPath:@"City.text"]];
        
        //        if ([[self.selectedData valueForKey:@"OfficeId"] isEqualToString:@""] && [[self.selectedData valueForKey:@"OfficeName"] isEqualToString:@""]) {
        
        [self.selectedData setValue:selectedOfficeId forKey:@"OfficeId"];
        [self.selectedData setValue:selectedOfficeName forKey:@"OfficeName"];
        BOOL isAllReadyExist = NO;
        
        for (int x = 0 ; self.arrMultiSelectTable3.count>x; x++) {
            NSDictionary *dico = [self.arrMultiSelectTable3 objectAtIndex:x];
            
         
            
            if ([[dico valueForKeyPath:@"OfficeId.text"]  isEqualToString:[dic valueForKeyPath:@"OfficeId.text"] ]) {
                
                isAllReadyExist = YES;
                
                [self.arrMultiSelectTable3 removeObjectAtIndex:x];
                break;
            }
        }
        
        if (!isAllReadyExist) {
            [self.arrMultiSelectTable3 addObject:dic];

        }
        //
        //        }else{
        //
        //            [self.selectedData setValue:@"" forKey:@"OfficeId"];
        //            [self.selectedData setValue:@"" forKey:@"OfficeName"];
        //
        //        }
        
        [self.table3 reloadData];

        
    } else if (tableView == _table4) {
        
        NSDictionary *dic = [self.arrTableFour objectAtIndex:indexPath.row];
        NSString *selectedOfficeId = [NSString stringWithFormat:@"%@", [dic valueForKeyPath:@"ClientId.text"]];
        NSString *selectedOfficeName = [NSString stringWithFormat:@"%@", [dic valueForKeyPath:@"Name.text"]];
        
        //        if ([[self.selectedData valueForKey:@"OfficeId"] isEqualToString:@""] && [[self.selectedData valueForKey:@"OfficeName"] isEqualToString:@""]) {
        
        [self.selectedData setValue:selectedOfficeId forKey:@"ClientId"];
        [self.selectedData setValue:selectedOfficeName forKey:@"ClientName"];
        BOOL isAllReadyExist = NO;
        
        for (int x = 0 ; self.arrMultiSelectTable4.count>x; x++) {
            NSDictionary *dico = [self.arrMultiSelectTable4 objectAtIndex:x];
            
            
            if ([[dico valueForKeyPath:@"ClientId.text"]  isEqualToString:[dic valueForKeyPath:@"ClientId.text"] ]) {
                
                isAllReadyExist = YES;
                
                [self.arrMultiSelectTable4 removeObjectAtIndex:x];
                break;
            }
        }
        
        if (!isAllReadyExist) {
            [self.arrMultiSelectTable4 addObject:dic];
            
        }

        //
        //        }else{
        //
        //            [self.selectedData setValue:@"" forKey:@"OfficeId"];
        //            [self.selectedData setValue:@"" forKey:@"OfficeName"];
        //
        //        }
        
        [self.table4 reloadData];
        
    }
    
    
}

- (IBAction)btnClearApps:(id)sender {
    
    [self.selectedData setValue:@"" forKey:@"AppId"];
    [self.selectedData setValue:@"" forKey:@"AppName"];
    [self.arrMultiSelectTable1 removeAllObjects];
    [self.table1 reloadData];
    
    
}

- (IBAction)btnClearMarkets:(id)sender {
    
    [self.selectedData setValue:@"" forKey:@"MarketId"];
    [self.selectedData setValue:@"" forKey:@"MarketName"];
    [self.arrMultiSelectTable2 removeAllObjects];

    [self.table2 reloadData];
}

- (IBAction)btnClearOffices:(id)sender {
    
    [self.selectedData setValue:@"" forKey:@"OfficeId"];
    [self.selectedData setValue:@"" forKey:@"OfficeName"];
    [self.arrMultiSelectTable3 removeAllObjects];

    [self.table3 reloadData];
}

- (IBAction)btnClearClient:(id)sender {
    
    [self.selectedData setValue:@"" forKey:@"ClientId"];
    [self.selectedData setValue:@"" forKey:@"ClientName"];
    [self.arrMultiSelectTable4 removeAllObjects];

    [self.table4 reloadData];
}

- (IBAction)btnApply:(id)sender {
    
    [self dismissViewControllerAnimated:true completion:^{
        
        
        [self.delegate refreshPage:self table1:self.arrMultiSelectTable1 table2:self.arrMultiSelectTable2 table3:self.arrMultiSelectTable3 table4:self.arrMultiSelectTable4];
        
    }];
    
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
