//
//  FilterVC.h
//  avispl
//
//  Created by Faizan on 07/09/16.
//  Copyright © 2016 iShadowX. All rights reserved.
//

#import <UIKit/UIKit.h>

@class FilterVC;

@protocol FilterDelegate <NSObject>

-(void)refreshPage:(FilterVC *)uploadVC table1:(NSMutableArray*)arrTable1 table2:(NSMutableArray*)arrTable2 table3:(NSMutableArray*)arrTable3 table4:(NSMutableArray*)aaTable4;
@end

@interface FilterVC : UIViewController

@property (nonatomic, assign) id <FilterDelegate> delegate;

@end
