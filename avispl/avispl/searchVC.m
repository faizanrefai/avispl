//
//  searchVC.m
//  avispl
//
//  Created by Faizan on 26/03/17.
//  Copyright © 2017 iShadowX. All rights reserved.
//

#import "searchVC.h"
#import "searchListCell.h"
#import "WebserviceCall.h"
#import "MBProgressHUD.h"
#import "avispl-Swift.h"
#import <AFNetworking/AFNetworking.h>
#import "UIKit+AFNetworking.h"
#import "FilterVC.h"
#import "DesktopVC.h"
#import "SettingVC.h"
#import "NSDictionary+valueAdd.h"

@interface searchVC ()<UITableViewDelegate, UITableViewDataSource, FilterDelegate, Settings, UISearchBarDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tblList;
@property (nonatomic,strong) NSMutableArray *arrProjects;
@property (nonatomic,strong) NSMutableArray *arrProjectsFilter;
@property (nonatomic,strong) UIPopoverController *popOver;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *btnFltr;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBarAVI;
@property (strong, nonatomic) NSMutableDictionary *dicFilter;
@property (weak, nonatomic) IBOutlet UILabel *lblNoResult;

@property (strong, nonatomic) NSURLSession *session;

@property (nonatomic, strong) UIRefreshControl *refreshControl;

@end

@implementation searchVC

//- (void)FlterVCViewControllerDidFinish:(FilterVC*)FilterVC
//{
//    NSMutableArray *arrbtnapply = FilterVC;
//    NSArray* someArray = secondViewController.someArray;
//    // Do something with the array
//}



- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.lblNoResult setHidden:YES];
    
    self.arrProjects = [[NSMutableArray alloc]init];
    self.arrProjectsFilter = [[NSMutableArray alloc]init];
    
    // Initialize the refresh control.
    self.refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl.backgroundColor = [UIColor colorWithRed:132.0/256.0 green:191.0/256.0 blue:65./256.0 alpha:1.0];
    self.refreshControl.tintColor = [UIColor whiteColor];
    NSMutableAttributedString *stringText = [[NSMutableAttributedString alloc] initWithString:@"Reloading..."];
    [stringText addAttribute: NSForegroundColorAttributeName value: [UIColor whiteColor] range: NSMakeRange(0, 12)];
    self.refreshControl.attributedTitle = stringText;
    [self.refreshControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
    [self.tblList addSubview:self.refreshControl];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:true];
    [self GetProjectList];
    
    self.dicFilter = [[NSMutableDictionary alloc]init];
    [ self.dicFilter addValueToDictionary:@"" andKeyIs:@"keyword"];
    [ self.dicFilter addValueToDictionary:@""  andKeyIs:@"clientId"];
    [ self.dicFilter addValueToDictionary:@"" andKeyIs:@"officeId"];
    [ self.dicFilter addValueToDictionary:@""  andKeyIs:@"marketId"];
    [ self.dicFilter addValueToDictionary:@""  andKeyIs:@"applicationId"];

}

- (void)GetProjectList {
    
    self.session = [WebserviceCall fetchServerResponse:nil callBackSelector:@selector(responseList:) andDelegate:self andwebMethod:@"ProjectList" andWebServiceMaster:@"GalleryService.asmx"];
    
}

- (void)refresh:(UIRefreshControl *)refreshControl {
    
    [self clearFilter];
    
}


-(void)sendDataToA:(NSArray *)array
{
    NSLog(@"%@", array);
}


- (void)responseList:(id)response {
    
    self.navigationItem.title = [NSString stringWithFormat:@"PROJECT LIST"];
    [self.refreshControl endRefreshing];
    [MBProgressHUD hideHUDForView:self.view animated:true];
    NSLog(@"%@", response);
    [self.arrProjects removeAllObjects];
    [self.arrProjectsFilter removeAllObjects];
    
    if ([[response valueForKey:@"ProjectModel"] isKindOfClass: [NSArray class]])
    {
        [self.arrProjects addObjectsFromArray:[response valueForKey:@"ProjectModel"]];
        [self.arrProjectsFilter addObjectsFromArray:[response valueForKey:@"ProjectModel"]];
    }
    else
    {
        if ([response count])
        {
            [self.arrProjects addObject:[response valueForKey:@"ProjectModel"]];
            [self.arrProjectsFilter addObject:[response valueForKey:@"ProjectModel"]];
        }
    }
    
    [self.tblList reloadData];
    
    //illi - start
    if (self.arrProjects.count > 0)
    {
        self.lblNoResult.hidden = YES;
    }
    else
    {
        self.lblNoResult.hidden = NO;
    }
    //illi - end
    
}

- (void)responseListFilter:(id)response {
    
    self.navigationItem.title = [NSString stringWithFormat:@"PROJECT LIST"];
    [self.refreshControl endRefreshing];
    [MBProgressHUD hideHUDForView:self.view animated:true];
    NSLog(@"%@", response);
    
    [self.arrProjects removeAllObjects];
    [self.arrProjectsFilter removeAllObjects];
    
    if ([[response valueForKey:@"ProjectModel"] isKindOfClass: [NSArray class]])
    {
        [self.arrProjects addObjectsFromArray:[response valueForKey:@"ProjectModel"]];
        [self.arrProjectsFilter addObjectsFromArray:[response valueForKey:@"ProjectModel"]];
    }
    else
    {
        if ([response count])
        {
            [self.arrProjects addObject:[response valueForKey:@"ProjectModel"]];
            [self.arrProjectsFilter addObject:[response valueForKey:@"ProjectModel"]];
        }
    }
    
    [self.tblList reloadData];
    
    //illi - start
    if (self.arrProjects.count > 0)
    {
        self.lblNoResult.hidden = YES;
    }
    else
    {
        self.lblNoResult.hidden = NO;
    }
    //illi - end
    
}


#pragma mark - Tableview delegate & datasource.

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.arrProjectsFilter.count;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    searchListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"searchListID"];
    NSDictionary *dic = [self.arrProjectsFilter objectAtIndex:indexPath.row];
        
    cell.lblAppName.text = [dic valueForKeyPath:@"ApplicationName.text"];
    cell.lblMarketName.text = [dic valueForKeyPath:@"MarketName.text"];
    cell.lblOfficeName.text = [dic valueForKeyPath:@"OfficeName.text"];
    cell.lblClientName.text = [dic valueForKeyPath:@"ClientName.text"];
    cell.lblProjectTitle.text = [dic valueForKeyPath:@"ProjectTile.text"];
    cell.lblProjectDescription.text = [dic valueForKeyPath:@"ProjectDescription.text"];
    [cell.imgThumb setImageWithURL:[NSURL URLWithString:[[NSString stringWithFormat:@"%@%@",ImageURL, [dic valueForKeyPath:@"Thuumbnail.text"]] urlEnocodeString]] placeholderImage:[UIImage imageNamed:@"placeholder-image"]];
    
    return cell;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSDictionary *dic = [self.arrProjectsFilter objectAtIndex:indexPath.row];
    [self performSegueWithIdentifier:@"projectDetailSeg" sender:dic];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"projectDetailSeg"])
    {
        DesktopVC *vc = [segue destinationViewController];
        vc.dicProjectDetail = sender;
    }
    
    if ([segue.identifier  isEqual: @"asdf"])
    {
        FilterVC *UploadingPost = [[(UINavigationController*)segue.destinationViewController viewControllers] firstObject];
        UploadingPost.delegate = self;
    }
    
    if ([segue.identifier isEqualToString:@"setting"])
    {
        SettingVC *setti = segue.destinationViewController;
        setti.handler  = self;
        
    }
    
}


- (IBAction)btnFiltr:(id)sender {
    
    if (self.btnFltr.tintColor == [UIColor greenColor])
    {
        [self clearFilter];
    }
    else
    {
        [self performSegueWithIdentifier:@"asdf" sender:nil];
    }
}



-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    
    
    [self.dicFilter addValueToDictionary:searchText andKeyIs:@"keyword"];
    
    //    if (searchText.length){
    //
    ////        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.ProjectTile.text contains[cd] %@",searchText];
    ////        self.arrProjectsFilter = [NSMutableArray arrayWithArray:[self.arrProjects filteredArrayUsingPredicate:predicate]];
    //
    ////        self.btnFltr.tintColor = [UIColor greenColor];
    ////        self.navigationItem.title = [NSString stringWithFormat:@"PROJECT LIST (%@)", searchText];
    //
    //
    //
    //    }else{
    ////        self.arrProjectsFilter = [NSMutableArray arrayWithArray:self.arrProjects ];
    //       
    //    }
    
    if (searchText.length) {
    
        self.navigationItem.title = [NSString stringWithFormat:@"LOADING..."];
        [self.session invalidateAndCancel];
        self.session = [WebserviceCall fetchServerResponse:self.dicFilter callBackSelector:@selector(responseListFilter:)
                                andDelegate:self andwebMethod:@"SearchForProjects" andWebServiceMaster:@"GalleryService.asmx"];
        
        
    } else {
    
        [self.session invalidateAndCancel];
        [self clearFilter];
        
    }
    
    

  }

- (void)clearFilter {
    
    [self.dicFilter addValueToDictionary:@""  andKeyIs:@"clientId"];
    [self.dicFilter addValueToDictionary:@"" andKeyIs:@"officeId"];
    [self.dicFilter addValueToDictionary:@""  andKeyIs:@"marketId"];
    [self.dicFilter addValueToDictionary:@""  andKeyIs:@"applicationId"];
    
    [self GetProjectList];
    self.navigationItem.title = [NSString stringWithFormat:@"PROJECT LIST"];
    self.btnFltr.tintColor = [UIColor whiteColor];
    self.searchBarAVI.text = @"";
    
}


-(void)logoutPressed{
    [self.navigationController popToRootViewControllerAnimated:true];
}

- (void)refreshPage:(FilterVC *)uploadVC table1:(NSMutableArray *)appID table2:(NSMutableArray *)marketID table3:(NSMutableArray *)officeID table4:(NSMutableArray *)clientID {
    
    NSArray *arrAppId = [appID valueForKeyPath:@"ApplicationId.text"];
    NSString *strAppId = [arrAppId componentsJoinedByString:@","];
    
    NSArray *arrMarketId = [marketID valueForKeyPath:@"MarketId.text"];
    NSString *strMarketId = [arrMarketId componentsJoinedByString:@","];

    NSArray *arrOfficeId = [officeID valueForKeyPath:@"OfficeId.text"];
    NSString *strOfficeId = [arrOfficeId componentsJoinedByString:@","];

    NSArray *arrClientId = [clientID valueForKeyPath:@"ClientId.text"];
    NSString *strClientId = [arrClientId componentsJoinedByString:@","];

    NSLog(@"App %@ Market %@ Office %@ Client %@",strAppId,strMarketId,strOfficeId,strClientId);
    
    self.btnFltr.tintColor = [UIColor greenColor];
//    self.navigationItem.title = [NSString stringWithFormat:@"PROJECT LIST (%@-%@-%@)", dict[@"AppName"], dict[@"MarketName"], dict[@"OfficeName"]];
//
//    
    [ self.dicFilter  addValueToDictionary:self.searchBarAVI.text andKeyIs:@"keyword"];
    
    [ self.dicFilter  addValueToDictionary:strAppId  andKeyIs:@"applicationId"];
    [ self.dicFilter  addValueToDictionary:strMarketId  andKeyIs:@"marketId"];
    [ self.dicFilter  addValueToDictionary:strOfficeId andKeyIs:@"officeId"];
    [ self.dicFilter  addValueToDictionary:strClientId  andKeyIs:@"clientId"];
    
    self.navigationItem.title = [NSString stringWithFormat:@"LOADING..."];
    
    [WebserviceCall fetchServerResponse:self.dicFilter callBackSelector:@selector(responseListFilter:)
                            andDelegate:self andwebMethod:@"SearchForProjects" andWebServiceMaster:@"GalleryService.asmx"];
    
    NSLog(@"%@", uploadVC);
}



@end
