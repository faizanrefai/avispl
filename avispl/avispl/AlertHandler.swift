//
//  AlertHandler.swift
//  ECO
//
//  Created by Faizan on 04/05/16.
//  Copyright © 2016 faizan_refai@yahoo.com. All rights reserved.
//

import UIKit

class AlertHandler: NSObject {
    
    class func showAlert(_ msg: String , vc: UIViewController){
    
       let alert =  UIAlertController(title: "", message: msg, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        vc.present(alert, animated: true, completion: nil)
        
    }

}
