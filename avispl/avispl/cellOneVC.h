//
//  cellOneVC.h
//  avispl
//
//  Created by Faizan on 07/09/16.
//  Copyright © 2016 iShadowX. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface cellOneVC : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblCellOne;
@property (weak, nonatomic) IBOutlet UILabel *lblCellTwo;
@property (weak, nonatomic) IBOutlet UILabel *lblCellThree;

@end
