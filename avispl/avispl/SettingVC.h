//
//  SettingVC.h
//  avispl
//
//  Created by Faizan on 13/04/17.
//  Copyright © 2017 iShadowX. All rights reserved.
//

#import <UIKit/UIKit.h>



@protocol Settings <NSObject>

-(void)logoutPressed;

@end



@interface SettingVC : UIViewController

@property (nonatomic,assign) id <Settings> handler;


@end
