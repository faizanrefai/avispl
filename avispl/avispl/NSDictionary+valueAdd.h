//
//  NSDictionary+valueAdd.h
//  memreas
//
//  Created by Faizan on 24/02/16.
//
//

#import <Foundation/Foundation.h>

@interface NSDictionary (valueAdd)
-(void)addValueToDictionary:(id)obj andKeyIs:(NSString*)aKey;
@end
