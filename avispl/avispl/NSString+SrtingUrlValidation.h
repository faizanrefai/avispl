/**
 * Copyright (C) 2015 memreas llc. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
//
//  NSString+SrtingUrlValidation.h
//  memreas
//

#import <Foundation/Foundation.h>

@interface NSString (SrtingUrlValidation)

-(BOOL)isValidURL;


-(id)convertToJson;
-(NSString*)convertToJsonWithFirstObject;
- (NSString*) urlEnocodeString ;



-(BOOL)isEqualToUpperCase:(NSString*)value;

@end
