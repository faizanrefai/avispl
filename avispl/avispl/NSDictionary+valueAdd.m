//
//  NSDictionary+valueAdd.m
//  memreas
//
//  Created by Faizan on 24/02/16.
//
//

#import "NSDictionary+valueAdd.h"

@implementation NSDictionary (valueAdd)

-(void)addValueToDictionary:(id)obj andKeyIs:(NSString*)aKey{
    
    @try {
        
        NSMutableDictionary *dic = (NSMutableDictionary*)self;
        NSString *strKey  = aKey?aKey:@"UndifinedData";
        
        if (obj) {
            [dic setObject:obj forKey:strKey];
        }else{
            [dic setObject:@"" forKey:strKey];
        }
        
    }
    @catch (NSException *exception) {
        NSLog(@"  addValueToDictionary  %@",exception);
    }
    
}


@end
