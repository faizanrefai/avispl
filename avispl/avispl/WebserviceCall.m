//
//  WebserviceCall.m
//  uTrompet
//
//  Created by Abdulkadir on 01/05/16.
//  Copyright © 2016 faizan_refai@yahoo.com. All rights reserved.
//

#import "WebserviceCall.h"
#import "AFNetworking.h"
#import "XMLReader.h"

#define webServiceURL @"https://ipad.avispl.com/Webservices"

@implementation WebserviceCall

/*
 
 <?xml version="1.0" encoding="utf-8"?>
 <soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">
 <soap12:Body>
 
 <LoginUser xmlns="http://tempuri.org/">
 <UserName>string</UserName>
 <Password>string</Password>
 <Mode>string</Mode>
 </LoginUser>
 </soap12:Body>
 </soap12:Envelope>
 
 */


+ (NSString *)generateXMLForInputDictionary:(NSDictionary*)input andWebMethod:(NSString*)webMethod{
    
    NSString* requestXML = @"<?xml version=\"1.0\" encoding=\"utf-8\"?><soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"><soap:Body>";
    if (input == nil) {
        requestXML = [requestXML stringByAppendingFormat:@"<%@ xmlns=\"http://tempuri.org/\" />",webMethod];
    }else{
        requestXML = [requestXML stringByAppendingFormat:@"<%@ xmlns=\"http://tempuri.org/\">",webMethod];
    }
    
    
    for (int x = 0; x<input.allKeys.count; x++) {
        NSString *key = [input.allKeys objectAtIndex:x];
        NSString *value = [input.allValues objectAtIndex:x];
        requestXML = [requestXML stringByAppendingFormat:@"<%@>%@</%@>",key,value,key];
    }
    
//    requestXML = [requestXML stringByAppendingFormat:@"<%@>%@</%@>",@"Mode",webMethod,@"Mode"];
    if (input == nil) {
//        requestXML = [requestXML stringByAppendingFormat:@"</%@>",webMethod];
    }else{
        requestXML = [requestXML stringByAppendingFormat:@"</%@>",webMethod];
    }
    
    requestXML = [requestXML stringByAppendingFormat:@"</soap:Body> </soap:Envelope>"];
    return requestXML;
    
}




+(NSMutableURLRequest*) generateWebServiceRequest:(NSString*)xmlRequestData webMasterPage:(NSString*)webMaster webMethod:(NSString*)method
{
    
    NSString *xmlRequestMessage = [NSString stringWithFormat:@"%@",xmlRequestData];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[xmlRequestMessage length]];
    NSString *urlString = [NSString stringWithFormat:@"%@/%@", webServiceURL,webMaster];
    //    NSLog(@"urlString---->%@",urlString);
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:urlString]];
//    [request setTimeoutInterval:120.0];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"text/xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
//    [request setValue:@"application/soap+xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [request setValue:[NSString stringWithFormat:@"http://tempuri.org/%@",method ] forHTTPHeaderField:@"SOAPAction"];
    [request setHTTPBody:[xmlRequestMessage dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSLog(@"SIZE OF Upload: %f Mb", [postLength floatValue]/1024/1024);

    
    return request;
}



+ (NSURLSession*)fetchServerResponse:(NSMutableDictionary*)inputDictionary callBackSelector:(SEL)callBackSelector andDelegate:(id)delegate andwebMethod:(NSString*)webMethod andWebServiceMaster:(NSString*)masterPage
{
    
    
    @try {
        
        NSString *requestXML = [WebserviceCall generateXMLForInputDictionary:inputDictionary  andWebMethod:webMethod];
        NSMutableURLRequest* request = [WebserviceCall generateWebServiceRequest:requestXML  webMasterPage:masterPage webMethod:webMethod];
        
        NSURLSession* session = [NSURLSession sharedSession];
        [[session dataTaskWithRequest:(NSURLRequest*)request
                    completionHandler:^(NSData* data, NSURLResponse* response,
                                        NSError* error) {
                        
                                            NSString *dataString = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
                                            NSLog(@"%@", dataString);
//                                            NSLog(@"dataTaskWithRequest::response::%@",response);
                                            NSLog(@"dataTaskWithRequest::error::%@",error);
                        if (!error) {
                            NSHTTPURLResponse* httpResp = (NSHTTPURLResponse*)response;
                            //                        NSLog(@"dataTaskWithRequest::httpResp.statusCode::%@",@(httpResp.statusCode));
                            if (httpResp.statusCode == 200) {
                                //                            NSLog(@"fetchServerResponse::response ---> %@",response);
                                
                                NSError *error;
                                
                                // Temporary Code for Error solve Start
//                                 NSString *dataString = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
//                                 NSLog(@"%@", dataString);
                                // Temporary Code for Error solve End
                                
                                NSDictionary*dicResponse ;
                                
                                dicResponse = [XMLReader dictionaryForXMLData:data error:&error];
                                
                                
                                
                                
                                if (dicResponse == nil) {
                                    
                                    dispatch_async(dispatch_get_main_queue(), ^{
                                        
                                        if ([delegate respondsToSelector:callBackSelector]) {
                                            [delegate performSelector:callBackSelector withObject:[NSDictionary dictionary]];
                                        }
                                        
                                    });

                                    
                                }else{
                                    
                                    
                                    NSString *respose = [NSString stringWithFormat:@"%@Response",webMethod];
                                    NSString *result = [NSString stringWithFormat:@"%@Result",webMethod];
                                    NSString *keyPath = [NSString stringWithFormat:@"soap:Envelope.soap:Body.%@.%@.text",respose,result];
                                    
                                    NSString*json  = [dicResponse valueForKeyPath:keyPath];
                                    NSDictionary *dic;;
                                    if (!json) {
                                        NSString *keyPath2 = [NSString stringWithFormat:@"soap:Envelope.soap:Body.%@.%@",respose,result];
                                        dic = [dicResponse valueForKeyPath:keyPath2];
                                    }else{
                                    
                                        
                                        dic = [NSJSONSerialization JSONObjectWithData:[json dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:nil];
                                        
                                    }
                                    
                                    
                                    //                                }
                                    dispatch_async(dispatch_get_main_queue(), ^{
                                        
                                        if (dic == nil) {
                                            if ([delegate respondsToSelector:callBackSelector]) {
                                                [delegate performSelector:callBackSelector withObject:json];
                                            }
                                            
                                        }else{
                                        
                                            if ([delegate respondsToSelector:callBackSelector]) {
                                                [delegate performSelector:callBackSelector withObject:dic];
                                            }
                                        }
                                       
                                        
                                    });
                                
                                }
                                
                                
                                
                            }else{
                            
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    
                                    if ([delegate respondsToSelector:callBackSelector]) {
                                        [delegate performSelector:callBackSelector withObject:[NSDictionary dictionary]];
                                    }
                                    
                                });

                            
                            }
                            
                            
                        } else {
                            NSLog(@"url error ---> %@", error);
                            
                            dispatch_async(dispatch_get_main_queue(), ^{
                                
                                if ([delegate respondsToSelector:callBackSelector]) {
                                    [delegate performSelector:callBackSelector withObject:[NSDictionary dictionary]];
                                }
                                
                            });
                            
                        }
                    }] resume];
        
        return session;
   
        
        
    }
    @catch (NSException *exception) {
        NSLog(@"Class : %@ :::  Method : %s %@",NSStringFromClass([self class]),__PRETTY_FUNCTION__,exception);
    }
    
  
    }


@end
