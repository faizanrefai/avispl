//
//  WebserviceCall.h
//  ECO
//
//  Created by Faizan on 01/05/16.
//  Copyright © 2016 faizan_refai@yahoo.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSString+SrtingUrlValidation.h"

#define  ImageURL @"https://ipad.avispl.com"

@interface WebserviceCall : NSObject

+ (NSURLSession*)fetchServerResponse:(NSMutableDictionary*)inputDictionary callBackSelector:(SEL)callBackSelector andDelegate:(id)delegate andwebMethod:(NSString*)webMethod andWebServiceMaster:(NSString*)masterPage;

@end
