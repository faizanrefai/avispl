//
//  MBProgressHUD.h
//  ECO
//
//  Created by Faizan on 24/05/16.
//  Copyright © 2016 faizan_refai@yahoo.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface MBProgressHUD : NSObject

+ (MBProgressHUD*)showHUDAddedTo:(UIView *)view animated:(BOOL)animated;
+ (BOOL)hideAllHUDsForView:(UIView *)view animated:(BOOL)animated;
+ (BOOL)hideHUDForView:(UIView *)view animated:(BOOL)animated;

@end
