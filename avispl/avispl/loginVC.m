    //
//  loginVC.m
//  avispl
//
//  Created by Faizan on 11/03/17.
//  Copyright © 2017 iShadowX. All rights reserved.
//

#import "loginVC.h"
#import "WebserviceCall.h"
#import "MBProgressHUD.h"
#import "avispl-Swift.h"

@interface loginVC ()
@property (weak, nonatomic) IBOutlet UITextField *txtEmail;
@property (weak, nonatomic) IBOutlet UITextField *txtPassword;

@end

@implementation loginVC

-(void)loadView{
    [super loadView];
    int isLOGIN = [[[NSUserDefaults standardUserDefaults] valueForKey:@"isLogin"] intValue];
    if (isLOGIN == 1) {
    
        [self performSegueWithIdentifier:@"segLogin" sender:nil];
        
    }
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)btnLoginPress:(id)sender {
    

    NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
    [dic setValue:self.txtEmail.text forKey:@"email"];
    [dic setValue:self.txtPassword.text forKey:@"password"];
    [WebserviceCall fetchServerResponse:dic callBackSelector:@selector(response:) andDelegate:self andwebMethod:@"AuthenticateUser" andWebServiceMaster:@"GalleryService.asmx"];
    [MBProgressHUD showHUDAddedTo:self.view animated:true];
    
}

-(void)response:(id)response{
    
    [MBProgressHUD hideHUDForView:self.view animated:true];
    
    if ([response isKindOfClass:[NSString class]]) {
    
        if ([response rangeOfString:@"successfully"].location != NSNotFound) {
            
            [[NSUserDefaults standardUserDefaults] setValue:@"1" forKey:@"isLogin"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [self performSegueWithIdentifier:@"segLogin" sender:nil];
            
        }else{
            
            [AlertHandler showAlert:@"Please enter correct email & Password." vc:self];
            
        }
        
    }else{
            [AlertHandler showAlert:@"Server Not Responding." vc:self];
    }
    
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
