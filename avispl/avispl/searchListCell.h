//
//  searchListCell.h
//  avispl
//
//  Created by Faizan on 26/03/17.
//  Copyright © 2017 iShadowX. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface searchListCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblProjectTitle;
@property (weak, nonatomic) IBOutlet UIImageView *imgThumb;
@property (weak, nonatomic) IBOutlet UILabel *lblClientName;
@property (weak, nonatomic) IBOutlet UILabel *lblAppName;
@property (weak, nonatomic) IBOutlet UILabel *lblMarketName;
@property (weak, nonatomic) IBOutlet UILabel *lblOfficeName;
@property (weak, nonatomic) IBOutlet UILabel *lblProjectDescription;

@end
