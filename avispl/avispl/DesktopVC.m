//
//  DesktopVC.m
//  avispl
//
//  Created by Faizan on 05/09/16.
//  Copyright © 2016 iShadowX. All rights reserved.
//

#import "DesktopVC.h"
#import "ProjectListCell.h"
#import "WebserviceCall.h"
#import <AFNetworking/AFNetworking.h>
#import "UIKit+AFNetworking.h"
#import "MBProgressHUD.h"
#import "SettingVC.h"


@interface DesktopVC () <UICollectionViewDelegate, UICollectionViewDataSource, UIScrollViewDelegate>

@property (strong, nonatomic) NSArray *ProjectContent;
@property (nonatomic, strong) NSMutableArray *arrImgList;
@property (weak, nonatomic) IBOutlet UIImageView *imgSelected;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomContraint;
@property (weak, nonatomic) IBOutlet UIView *viewHide;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UICollectionView *imgListCollection;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UITextView *lblDescription;
@property (weak, nonatomic) IBOutlet UIButton *btnPlayPause;

@property NSInteger currentImgIndexSwipe;
@property NSInteger slider;
@property NSTimer* timer;

@end

@implementation DesktopVC

- (void)viewDidLoad {

    [super viewDidLoad];
    
    
    [self.btnPlayPause setHidden:YES];

    self.scrollView.zoomScale = 1;
    self.scrollView.maximumZoomScale = 10;
    
    self.currentImgIndexSwipe = 0;
    
    NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
    [dic setValue:[self.dicProjectDetail valueForKeyPath:@"ProjectId.text"] forKey:@"projectId"];
    
    [WebserviceCall fetchServerResponse:dic callBackSelector:@selector(responseImgList:) andDelegate:self andwebMethod:@"GetImages" andWebServiceMaster:@"GalleryService.asmx"];
    [MBProgressHUD showHUDAddedTo:self.view animated:true];

    self.navigationItem.title = [[self.dicProjectDetail valueForKey:@"ProjectTile"] valueForKey:@"text"];
    
}

-(void)responseImgList:(NSDictionary*)response{
    
    [MBProgressHUD hideHUDForView:self.view animated:true];
    
    if ([response valueForKey:@"PhotoModel"])
    {
        if ([[response valueForKey:@"PhotoModel"] isKindOfClass:[NSDictionary class]])
        {
            self.arrImgList = [[NSMutableArray alloc]init];
            [self.arrImgList addObject:[response valueForKey:@"PhotoModel"]];
        }
        else
        {
            self.arrImgList = [[NSMutableArray alloc]initWithArray:[response valueForKey:@"PhotoModel"]];
        }
        
        if (self.arrImgList.count > 1)
        {
            [self.btnPlayPause setHidden:NO];
        }
        else
        {
            [self.btnPlayPause setHidden:YES];
        }
        
        NSDictionary *dic = [self.arrImgList objectAtIndex:0];
        [self.imgSelected setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@", ImageURL,[dic valueForKeyPath:@"CoverImage.text"]]] placeholderImage:[UIImage imageNamed:@"loading"]];
        self.lblTitle.text = [dic valueForKeyPath:@"Title.text"];
        //self.lblDescription.text = [dic valueForKeyPath:@"ShortDescription.text"];
        self.lblDescription.attributedText = [self convertHtmlToString:[dic valueForKeyPath:@"ShortDescription.text"]];
        [self.imgListCollection reloadData];
        
    } else {
        
        [self.navigationController popViewControllerAnimated:true];
    
    }
    
}

-(UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView{
    
    return self.imgSelected;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)HideShow:(UITapGestureRecognizer*)recogonizer {
    
    [UIView animateWithDuration:0.7f animations:^
    {
        if (self.bottomContraint.constant == 250.0f)
        {
            [self.bottomContraint setConstant:800.0f];
            [self.viewHide setAlpha:0.0f];
        }
        else
        {
            [self.bottomContraint setConstant:250.0f];
            [self.viewHide setAlpha:1.0f];
        }
        [self.view layoutIfNeeded];
    }];
    
}



- (IBAction)zoom:(UIPinchGestureRecognizer *)recognizer {
    
    if (recognizer.scale >1.0f && recognizer.scale < 5.5f)
    {
        CGAffineTransform transform = CGAffineTransformMakeScale(recognizer.scale, recognizer.scale);
        self.imgSelected.transform = transform;
    }
    
}



- (IBAction)NextImage:(UISwipeGestureRecognizer *)recognizer {
    
    CATransition *transition = [CATransition animation];
    transition.duration = 1.0f;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionFade;
    
    [self.imgSelected.layer addAnimation:transition forKey:nil];
    //[self.imgSelected.layer addAnimation:[CATransition animation] forKey:kCATransition];
    
    if (self.currentImgIndexSwipe < self.arrImgList.count-1)
    {
        self.currentImgIndexSwipe = self.currentImgIndexSwipe + 1;
        NSDictionary *dic = self.arrImgList[self.currentImgIndexSwipe];
        [self.imgSelected setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@", ImageURL,[dic valueForKeyPath:@"CoverImage.text"]]] placeholderImage:[UIImage imageNamed:@"loading"]];
        self.lblTitle.text = [dic valueForKeyPath:@"Title.text"];
        //self.lblDescription.text = [dic valueForKeyPath:@"ShortDescription.text"];
        self.lblDescription.attributedText = [self convertHtmlToString:[dic valueForKeyPath:@"ShortDescription.text"]];
        //self.imgSelected.image = [UIImage imageNamed:[self.imgList objectAtIndex:self.imgswipe]];
    }
    else
    {
        //self.imgswipe = 0;
        //self.imgSelected.image = [UIImage imageNamed:[self.imgList objectAtIndex:self.imgswipe]];
        //self.imgswipe = self.imgswipe + 1;
        
    }
}





- (IBAction)btnPlayPressed:(UIButton*)sender {
    
    
            if (self.currentImgIndexSwipe < self.arrImgList.count-1)
            {
                if (self.btnPlayPause.selected)
                {
                    //pause code
                    
                    [self.btnPlayPause setImage:[UIImage imageNamed:@"play"] forState:UIControlStateNormal];
                    self.btnPlayPause.selected = NO;
                    [self.timer invalidate];
                    
                    [UIView animateWithDuration:0.7f animations:^
                    {
                        [self.bottomContraint setConstant:250.0f];
                        [self.viewHide setAlpha:1.0f];[self.view layoutIfNeeded];
                    }];
                    
                }
                else
                {
                    //play code
                    
                    self.btnPlayPause.selected = YES;
                    [self.btnPlayPause setImage:[UIImage imageNamed:@"pause"] forState:UIControlStateNormal];
                    
                    [UIView animateWithDuration:0.7f animations:^
                    {
                        [self.bottomContraint setConstant:800.0f];
                        [self.viewHide setAlpha:0.0f];
                        [self.view layoutIfNeeded];
                    }];
                    
                    self.slider = 0;
                    self.timer = [NSTimer scheduledTimerWithTimeInterval:5.0 target:self selector:@selector(changeImage) userInfo:nil repeats:YES];
                }
                
            }
            else
            {
                self.currentImgIndexSwipe = 0;
                
                //play code
                
                self.btnPlayPause.selected = true;
                [self.btnPlayPause setImage:[UIImage imageNamed:@"pause"] forState:UIControlStateNormal];
                
                [UIView animateWithDuration:0.7f animations:^{
                    [self.bottomContraint setConstant:800.0f];
                    [self.viewHide setAlpha:0.0f];
                    [self.view layoutIfNeeded];
                }];
                
                self.slider = 0;
                self.timer = [NSTimer scheduledTimerWithTimeInterval:5.0 target:self selector:@selector(changeImage) userInfo:nil repeats:YES];
            }

    
    
    
//    if (self.btnPlayPause.selected) {
//        
//        //pause code
//        
//        [self.btnPlayPause setImage:[UIImage imageNamed:@"play"] forState:UIControlStateNormal];
//        self.btnPlayPause.selected = false;
//        
//        [self.timer invalidate];
//        
//        [UIView animateWithDuration:0.7f animations:^{
//            [self.bottomContraint setConstant:250.0f];
//            [self.viewHide setAlpha:1.0f];[self.view layoutIfNeeded];
//        }];
//
//        
//    }else
//        
//    {
//        //play code
//        
//        self.btnPlayPause.selected = true;
//        [self.btnPlayPause setImage:[UIImage imageNamed:@"pause"] forState:UIControlStateNormal];
//        
//        [UIView animateWithDuration:0.7f animations:^{
//            [self.bottomContraint setConstant:800.0f];
//            [self.viewHide setAlpha:0.0f];
//            [self.view layoutIfNeeded];
//        }];
//        
//        self.slider = 0;
//        self.timer = [NSTimer scheduledTimerWithTimeInterval:2.0 target:self selector:@selector(changeImage) userInfo:nil repeats:YES];
//        
//        if (self.imgswipe < self.imgList.count-1){}else{
//            self.imgswipe = 0;
//        }
//
//
//    }
    
    
    
}

-(void)viewWillDisappear:(BOOL)animated{

    [self.timer invalidate];
    
}


- (void) changeImage {
    
    self.slider = self.slider + 1;
    
    CATransition *transition = [CATransition animation];
    transition.duration = 1.0f;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionFade;
    
    [self.imgSelected.layer addAnimation:transition forKey:nil];
    
    if (self.currentImgIndexSwipe < self.arrImgList.count-1)
    {
        
        self.currentImgIndexSwipe = self.currentImgIndexSwipe + 1;
        NSDictionary *dic = self.arrImgList[self.currentImgIndexSwipe];
        [self.imgSelected setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@", ImageURL,[dic valueForKeyPath:@"CoverImage.text"]]] placeholderImage:[UIImage imageNamed:@"loading"]];
        self.lblTitle.text = [dic valueForKeyPath:@"Title.text"];
        //self.lblDescription.text = [dic valueForKeyPath:@"ShortDescription.text"];
        self.lblDescription.attributedText = [self convertHtmlToString:[dic valueForKeyPath:@"ShortDescription.text"]];
    }
    else
    {
        
        //[self.timer invalidate];
        //[self.btnPlayPause setImage:[UIImage imageNamed:@"play"] forState:UIControlStateNormal];
        //[UIView animateWithDuration:0.7f animations:^{
        //            [self.bottomContraint setConstant:250.0f];
        //            [self.viewHide setAlpha:1.0f];[self.view layoutIfNeeded];
        //}];
        
        // For continue slide show again.
        self.currentImgIndexSwipe = 0;
        self.slider = 0;
        NSDictionary *dic = self.arrImgList[self.currentImgIndexSwipe];
        [self.imgSelected setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@", ImageURL,[dic valueForKeyPath:@"CoverImage.text"]]] placeholderImage:[UIImage imageNamed:@"loading"]];
        self.lblTitle.text = [dic valueForKeyPath:@"Title.text"];
        //self.lblDescription.text = [dic valueForKeyPath:@"ShortDescription.text"];
        self.lblDescription.attributedText = [self convertHtmlToString:[dic valueForKeyPath:@"ShortDescription.text"]];
        
    }
    
    
}


- (IBAction)PreviousImage:(UISwipeGestureRecognizer *)recognizer {
    
    CATransition *transition = [CATransition animation];
    transition.duration = 1.0f;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionFade;
    
    [self.imgSelected.layer addAnimation:transition forKey:nil];
    
    if (self.currentImgIndexSwipe != 0){
        
        self.currentImgIndexSwipe = self.currentImgIndexSwipe - 1;
        
        NSDictionary *dic = self.arrImgList[self.currentImgIndexSwipe];
        [self.imgSelected setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@", ImageURL,[dic valueForKeyPath:@"CoverImage.text"]]] placeholderImage:[UIImage imageNamed:@"loading"]];
        self.lblTitle.text = [dic valueForKeyPath:@"Title.text"];
        //self.lblDescription.text = [dic valueForKeyPath:@"ShortDescription.text"];
        self.lblDescription.attributedText = [self convertHtmlToString:[dic valueForKeyPath:@"ShortDescription.text"]];
        
    }
    
}




-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return self.arrImgList.count;
    
}


-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    
    ProjectListCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"imgListItem" forIndexPath:indexPath];
    NSDictionary *dic = self.arrImgList[indexPath.row];
    cell.imgContent.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    cell.imgContent.layer.borderWidth = 1;
    [cell.imgContent setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@", ImageURL,[dic valueForKeyPath:@"Thumbnail.text"]]] placeholderImage:[UIImage imageNamed:@"loadingSmall"]];
    
    self.imgSelected.tag = indexPath.row;

    
    return cell;
    
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(nonnull NSIndexPath *)indexPath{
    
    
    self.currentImgIndexSwipe = indexPath.row;
    self.scrollView.zoomScale = 1;
    NSDictionary *dic = self.arrImgList[indexPath.row];
    [self.imgSelected setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@", ImageURL,[dic valueForKeyPath:@"CoverImage.text"]]] placeholderImage:[UIImage imageNamed:@"loading"]];
    self.lblTitle.text = [dic valueForKeyPath:@"Title.text"];
    //self.lblDescription.text = [dic valueForKeyPath:@"ShortDescription.text"];
    self.lblDescription.attributedText = [self convertHtmlToString:[dic valueForKeyPath:@"ShortDescription.text"]];

    
}


-(NSMutableAttributedString*) convertHtmlToString:(NSString*)htmlString {

    
    //htmlString = @"<h1>Header</h1><h2>Subheader</h2><p>Some <em>text</em></p><img src='http://blogs.babble.com/famecrawler/files/2010/11/mickey_mouse-1097.jpg' width=70 height=100 />";
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc]
                                            initWithData: [htmlString dataUsingEncoding:NSUnicodeStringEncoding]
                                            options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType }
                                            documentAttributes: nil
                                            error: nil
                                            ];
    [attributedString addAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor], NSFontAttributeName: [UIFont systemFontOfSize:16.0]} range:NSMakeRange(0, attributedString.length)];
    return attributedString;
    
    //textView.attributedText = attributedString;
    
    
}



//#pragma mark - Navigation
//
//// In a storyboard-based application, you will often want to do a little preparation before navigation
//- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
//    // Get the new view controller using [segue destinationViewController].
//    // Pass the selected object to the new view controller.
//    
//    if ([segue.identifier isEqualToString:@"setting"]) {
//     
//        SettingVC *setti = segue.destinationViewController;
//        setti.handler  = self;
//        
//    }
//}


@end
